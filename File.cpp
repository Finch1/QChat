#include "File.hpp"

DataStream::DataStream()
{
	IsRead = false;
	in_ios.clear();
}

DataStream::DataStream(const char * Src, int Size,bool copy)
{
	IsRead = true;
	PutFrom(Src, Size,copy);
}

DataStream::DataStream(const string & Src, bool copy)
{
	IsRead = true;
	PutFrom(Src);
}

DataStream::~DataStream()
{
	in_ios.clear();
}

void DataStream::DoString(string & Str)
{
	int cnt = 0;
	char tmp;

	if (IsRead)
	{
		Str = "";
		DoObj<int>(cnt);
		rep(i, 0, cnt - 1)
		{
			DoObj<char>(tmp);
			Str += tmp;
		}
	}
	else
	{
		cnt = Str.length();
		DoObj<int>(cnt);
		rep(i, 0, cnt - 1)
		{
			DoObj<char>(Str[i]);
		}
	}
}

int DataStream::Size()
{
	return in_ios.str().length();
}

void DataStream::Clear()
{
	in_ios.clear();
}

string DataStream::GetSrc()
{
	return in_ios.str();
}

const char * DataStream::GetCCS()
{
	int size_ = Size();
	char *tmp = new char[size_];
	string ss = GetSrc();
	rep(i, 0, size_-1) {
		tmp[i] = ss.at(i);
		//if (tmp[i] == 0)tmp[i] = 1;
	}
	return tmp;
}

void DataStream::PutFrom(const char * Src, int Size,bool copy)
{
	Clear();
	rep(i, 0, Size - 1)
	{
		//if (Src[i] == 1)Src[i] = 0;
		in_ios << Src[i];
	}
	SetPoint(0);
}

void DataStream::PutFrom(const string & Src)
{
	Clear();
	int Size = Src.length();
	rep(i, 0, Size - 1)
	{
		//if (Src[i] == 1)Src[i] = 0;
		in_ios << Src.at(i);
	}
	SetPoint(0);
}
/*
int DataStream::CopyTo(char ** output)
{
	if (output != nullptr)delete output;
	string tmp = GetSrc();
	int Size = tmp.length();
	output = new char *[Size];
	rep(i, 0, Size - 1) {
		output[0][i] = tmp.at(i);
	}
	return Size;
}
*/
int DataStream::CopyTo(string & output)
{
	output = "";
	string tmp = GetSrc();
	output.assign(tmp.begin(), tmp.end());
	return tmp.length();
}

void DataStream::ChangeMode(bool IsRead_)
{
	SetPoint(0);
	IsRead = IsRead_;
}

bool DataStream::GetMode()
{
	return IsRead;
}

void DataStream::SetPoint(int p)
{
	in_ios.seekg(p);
}
