#pragma once

#include "File.hpp"

#include <map>
#include <thread>
#include <mutex>

#if defined(_WIN32)
#include <direct.h>
//#pragma execution_character_set("utf-8")
#include "winsock2.h"
//#pragma comment(lib, "WS2_32.lib")
#define HSocket SOCKET
#else
#include<memory.h>
#include<dlfcn.h>
#include<dirent.h>
#include<unistd.h>
#include<fcntl.h>
#include<arpa/inet.h>		// for inet_**
#include<netdb.h>			// for gethost**
#include<netinet/in.h>		// for sockaddr_in
#include<sys/types.h>		// for socket
#include<sys/socket.h>		// for socket
#define HSocket int
#endif

using namespace std;

	namespace Network {
		static const float MSG_UPDATA_INTERVAL = 0.01f;  //毫秒
		static const int MAX_CONNECT = 5;
		static const int MAX_BUFFER = 4096;

		enum MessageType
		{
			Message_DISCONNECT,
			Message_RECEIVE,
			Message_NEW_CONNECTION,
			Message_ONERROR
		};

		enum ErrorCode {
			Error_Normal,
			Error_Rev,
			Error_Init,
			Error_Send,
			Error_Sever_Bind,
			Error_Sever_Listen,
			Error_Sever_Accept
		};

		class SocketBase
		{
		public:
			SocketBase();
			~SocketBase();
			bool nonBlock(HSocket socket);
		protected:
			void closeConnect(HSocket socket);
			bool error(HSocket socket);

			std::mutex _mutex;
		private:
			bool _bInitSuccess;
		};

		class SocketMessage:public DataStream
		{
		private:
			MessageType msgType;
			HSocket socketClient;
		public:
			SocketMessage(MessageType type, HSocket socket_, char * data, int dataLen);
			SocketMessage(MessageType type, HSocket socket_);
			SocketMessage(MessageType type);
			SocketMessage(MessageType type, char * data, int dataLen);
			MessageType getMsgType();
			HSocket getSocket();
			//SOCKET getIt();
			~SocketMessage();
		};

		class SocketServer : public SocketBase
		{
		public:

			static SocketServer* getInstance();
			void destroyInstance();

			bool startServer(unsigned short port, bool UpdateInside = true, bool isAsyn = false);
			void sendMessage(HSocket it, shared_ptr<DataStream> dm);
			void sendMessage(shared_ptr<DataStream> dm);

			std::function<void(const char* ip)> onStart;
			std::function<void(HSocket it)> onNewConnection;
			std::function<void(HSocket it, shared_ptr<DataStream> dm)> onRecv;
			std::function<void(HSocket it)> onDisconnect;
			std::function<void(ErrorCode ErrorCode)> onError;

			void update();

		protected:
			SocketServer();
			~SocketServer();

		private:
			void clear();
			bool initServer(unsigned short port);
			void acceptClient();
			void acceptrepnc();
			void startWork(bool isAsyn);	//是否是同步消息处理(在主线程里处理消息)
			void Workrepnc();	//异步消息处理回调
			void newClientConnected(HSocket it);
			void recvMessage(HSocket it);

		private:
			static SocketServer* s_server;
			HSocket _socketServer;
			unsigned short _serverPort;

		private:
			std::map<HSocket,bool> _clientSockets;
			std::list<SocketMessage*> _UIMessageQueue;
			std::mutex   _UIMessageQueueMutex;
			std::mutex _ClientListMutex;
		};

		class SocketClient : public SocketBase
		{
		public:
			static SocketClient* getInstance();
			void destroy();

			bool connectServer(const char* serverIP, unsigned short port, bool UpdateInside=true,bool isAsyn = false);
			void sendMessage(shared_ptr<DataStream> dm);

			std::function<void()> onConnect;
			std::function<void(shared_ptr<DataStream> dm)> onRecv;
			std::function<void()> onDisconnect;
			std::function<void(ErrorCode Code)> onError;

			void update(float dt);//if UpdateInside  == false   ,must be call for this repnction

		protected:
			SocketClient(void);
			~SocketClient(void);

		private:
			bool initClient();
			void startWork(bool isAsyn);//mark:  just call for it inside
			void Workrepnc();//mark:  just call for it inside
			void recvMessage();
			void clear();

		private:
			static SocketClient *in_socket;

			HSocket _socketServer;
			HSocket _socektClient;
			std::list<SocketMessage*> _UIMessageQueue;
			std::mutex   _UIMessageQueueMutex;
		};
	}
